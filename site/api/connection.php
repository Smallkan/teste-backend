<?php 

class Connection {

	public $pdo = null;

	private $con = [
		'driver' => 'mysql',
		'host' => 'localhost',
		'db' => 'desafio',
		'user' => 'root',
		'pwd' => '',
		'port' => ''
	];

	function Connection() {
   		
       $this->pdo = $this->connects();

   	}


   	private function connects(){
		$strCon = "{$this->con['driver']}:host={$this->con['host']};dbname={$this->con['db']}";
		try {
	    	$dbh = new PDO($strCon, $this->con['user'], $this->con['pwd']);
	    	$dbh->setAttribute(PDO::ATTR_PERSISTENT, PDO::FETCH_OBJ);
	    	return $dbh;
		} catch (PDOException $e) {
		    print "Error!: " . $e->getMessage();
		    die();
		}
   	}
}

?>