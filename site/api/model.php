<?php 
require_once 'connection.php';

class Model{

	private $con = null;
	private $regionals = null, $national = null, $data = null;

	function __construct(){
		$connection = new Connection;
		$this->con = $connection->pdo;
		if($this->con == null){
			echo "Erro na conexão com o banco de dados!";
			exit;
		}else{
			$this->mediaRegional();
			$this->mediaNacional();
			echo $this->mountData();
		}
			
	}


	// regional
	function mediaRegional()
	{
		// total de ex-alunos
		$sql = "SELECT COUNT(a.id) AS totalExAlunos, c.regional FROM answers a 
				INNER JOIN alternatives b ON a.alternative_id = b.id
				INNER JOIN students c ON a.student_id = c.id
				GROUP BY c.regional";

		$stmt = $this->con->prepare($sql);
		$stmt->execute();
		$totalExAlunos = $stmt->fetchAll();
		$stmt->closeCursor();

		// total de ex-alunos que disseram "sim".
		$sql = "SELECT COUNT(a.id) AS totalExEstudando, c.regional FROM answers a 
				INNER JOIN alternatives b ON a.alternative_id = b.id
				INNER JOIN students c ON a.student_id = c.id
				WHERE a.alternative_id=37 GROUP BY c.regional";

		$stmt = $this->con->prepare($sql);
		$stmt->execute();
		$totalExEstudando = $stmt->fetchAll();
		$stmt->closeCursor();

		$i=0;
		foreach ($totalExAlunos as $exAlunos) {
			foreach ($totalExEstudando as $exEstudando) {
				if($exAlunos['regional'] == $exEstudando['regional']){
					$this->regionals['regionals'][$i] = [
						'description' => $exEstudando['regional'],
						'average' => number_format(($exEstudando['totalExEstudando']/$exAlunos['totalExAlunos'])*100, 4)
					];
					$i++;
				}
			}
		}

	}

	// nacional
	function mediaNacional()
	{
		// total de ex-alunos
		$sql = "SELECT COUNT(a.id) AS totalExAlunos FROM answers a 
				INNER JOIN alternatives b ON a.alternative_id = b.id
				INNER JOIN students c ON a.student_id = c.id";

		$stmt = $this->con->prepare($sql);
		$stmt->execute();
		$totalExAlunos = $stmt->fetchAll();
		$stmt->closeCursor();

		// total de ex-alunos que disseram "sim".
		$sql = "SELECT COUNT(a.id) AS totalExEstudando FROM answers a 
				INNER JOIN alternatives b ON a.alternative_id = b.id
				INNER JOIN students c ON a.student_id = c.id
				WHERE a.alternative_id=37";

		$stmt = $this->con->prepare($sql);
		$stmt->execute();
		$totalExEstudando = $stmt->fetchAll();
		$stmt->closeCursor();

		$this->national = [
			'national' => number_format(($totalExEstudando[0]['totalExEstudando']/$totalExAlunos[0]['totalExAlunos'])*100, 4)
		];

	}

	// mesclando os dados e gerando o JSON.
	function mountData()
	{
		if($this->regionals !== null && $this->national !== null){
			$this->data = array_merge($this->regionals, $this->national);
			return json_encode($this->data);
		}else{
			$error = "Erro ao tentar processar as informações!";
			return $error;
		}

	}
}

?>