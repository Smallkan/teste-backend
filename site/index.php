<!DOCTYPE html>
<html lang="pt-br">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Teste</title>

    <!-- bootstrap cdn -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <!-- css custom -->
    <link rel="stylesheet" type="text/css" href="css/custom.css">

    <!-- scripts -->
    <script src="https://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
    crossorigin="anonymous"></script>

  </head>
  <body>

    <div class="container mt-4">
      <div align="center">
        
        <h1 class="mb-4">Bem-Vindo(a)!</h1>

        <hr>
        <button id="loadApi" class="btn btn-primary">Buscar API</button>
        
        <div id="data" class="mt-4"></div>
      </div>
    </div>

    <script src="js/functions.js"></script>
</body>
</html>

